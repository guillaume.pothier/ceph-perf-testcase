# Ceph performance testcase

Creates two EC2 instances on AWS using Terraform:

* PVE: a t3a.xlarge (16 GB) instance to run Proxmox VE with Ceph
* Ceph client: currently t3a.nano (512 MB), can be modified to test
  with different RAM sizes (see the `ceph_client_instance_type`
  variable in `infrastructure/02-variables.tf`). 

## Requirements

* `jq` (eg. `sudo apt-get install -y jq`)
* [Terraform](https://developer.hashicorp.com/terraform/downloads).
* An AWS account.

## Create infrastructure with Terraform

Select AWS profile:

```bash
export AWS_PROFILE=<profile name, eg. playground>
export AWS_DEFAULT_REGION=<aws region, eg. us-west-2>
```

Run terraform:

```bash
./create-infrastructure.sh
```

If your Terraform binary is not named `terraform`, set the `TF_BIN` 
variable to the correct name, eg.:

```bash
TF_BIN=terraform-1.3.3 ./create-infrastructure.sh
```

## Install Proxmox and Ceph

This will ssh into the created EC2 instances and perform the required
setup (install package, configure Ceph, etc.)

```bash
./install-ceph.sh
```

## Run the test case

This will pipe `/dev/urandom` into an RBD volume, through `pv`
so that the throughput is displayed.

```bash
./run-testcase.sh
```

## Destroy infrastructure

```bash
./destroy-infrastructure.sh
```
