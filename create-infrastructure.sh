#! /bin/sh

set -e

: "${TF_BIN:=terraform}"

if [ -e sshkey ]
then
    echo "SSH key already exists"
else
	echo "Creating SSH access key"
    ssh-keygen -b 2048 -t rsa -f sshkey -q -N ""
fi

${TF_BIN} -chdir=infrastructure init

${TF_BIN} -chdir=infrastructure plan \
	-var "ssh_public_key=$(cat sshkey.pub)" \
	-out=plan.tfplan

${TF_BIN} -chdir=infrastructure apply plan.tfplan

${TF_BIN} -chdir=infrastructure show -json > state.json
