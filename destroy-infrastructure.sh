#! /bin/sh

set -e

: "${TF_BIN:=terraform}"

${TF_BIN} -chdir=infrastructure destroy \
	-var "ssh_public_key=NotNeeded"
