data "aws_availability_zones" "available_azs" {
  state = "available"
}

locals {
  selected_az = data.aws_availability_zones.available_azs.names[0]
}

variable "debian_ami_owner" {
  type        = string
  description = "Owner account of the Debian AMI"
  default     = "136693071363"
}

variable "debian_ami_name" {
  type        = string
  description = "Name of Debian Bullseye AMI"
  default     = "debian-11-amd64-20221020-1174"
}

variable "ssh_public_key" {
  type        = string
  description = "A public key for SSH access"
}

variable "ceph_client_instance_type" {
  type        = string
  description = "Instance type to be used for the Ceph client"
  default     = "t3a.nano"
}

variable "pve_instance_type" {
  type        = string
  description = "Instance type to be used for pve"
  default     = "t3a.xlarge"
}

variable "pve_initial_ceph_storage_size" {
  type        = number
  description = "Size in GB of storage volume for PVE"
  default     = 1000
}
