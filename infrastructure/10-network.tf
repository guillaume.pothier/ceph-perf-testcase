module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"

  name = "ceph-perf-testcase"
  cidr = "10.0.0.0/16"

  azs             = [local.selected_az]
  private_subnets = ["10.0.65.0/24"]
  public_subnets  = ["10.0.129.0/24"]

  enable_dns_hostnames = true
  enable_dns_support   = true
}

resource "aws_route53_zone" "private" {
  name = "ceph-perf-testcase"

  vpc {
    vpc_id = module.vpc.vpc_id
  }
}
