data "aws_ami" "debian" {
  owners      = [var.debian_ami_owner]
  most_recent = true

  filter {
    name   = "name"
    values = [var.debian_ami_name]
  }
}

resource "aws_key_pair" "ssh_key" {
  key_name   = "ssh-key"
  public_key = var.ssh_public_key
}

module "pve_instance" {
  source = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 4.0"

  name = "ceph-perf-testcase-pve"

  ami                         = data.aws_ami.debian.id
  instance_type               = var.pve_instance_type
  availability_zone           = local.selected_az
  subnet_id                   = module.vpc.public_subnets[0]
  vpc_security_group_ids      = [module.pve_security_group.security_group_id]
  associate_public_ip_address = true
  enable_volume_tags          = false

  key_name = aws_key_pair.ssh_key.key_name

  root_block_device = [
    {
      encrypted             = true
      volume_type           = "gp3"
      volume_size           = 20
      delete_on_termination = false
    },
  ]
}

module "pve_security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "ceph-perf-testcase-pve"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  egress_rules        = ["all-all"]
  ingress_rules       = ["ssh-tcp", "all-icmp"]

  ingress_with_cidr_blocks = [
    {
      from_port   = 8006
      to_port     = 8006
      protocol    = "tcp"
      description = "Proxmox Virtualization Environment"
    }, {
      cidr_blocks = "10.0.0.0/16"
      from_port   = 3300
      to_port     = 7300
      protocol    = "tcp"
      description = "Ceph"
    },
  ]
}


resource "aws_route53_record" "pve_private" {
  zone_id = aws_route53_zone.private.id
  name    = "pve.ceph-perf-testcase"
  type    = "A"
  ttl     = "300"
  records = [module.pve_instance.private_ip]
}

