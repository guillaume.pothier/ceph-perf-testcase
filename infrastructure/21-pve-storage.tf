resource "aws_ebs_volume" "ceph_storage" {
  availability_zone = local.selected_az
  encrypted         = true

  type = "gp3"
  size = var.pve_initial_ceph_storage_size
}

resource "aws_volume_attachment" "ceph_storage" {
  device_name = "/dev/sdf"
  volume_id   = aws_ebs_volume.ceph_storage.id
  instance_id = module.pve_instance.id
}

