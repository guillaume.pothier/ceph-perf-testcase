module "ceph_client_instance" {
  source = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "ceph-perf-testcase-pve-ceph-client"

  ami                         = data.aws_ami.debian.id
  instance_type               = var.ceph_client_instance_type
  availability_zone           = local.selected_az
  subnet_id                   = module.vpc.public_subnets[0]
  vpc_security_group_ids      = [module.ceph_client_security_group.security_group_id]
  associate_public_ip_address = true

  key_name = aws_key_pair.ssh_key.key_name
}

module "ceph_client_security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "ceph-perf-testcase-pve-ceph-client"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  egress_rules        = ["all-all"]
  ingress_rules       = ["ssh-tcp", "all-icmp"]
}

resource "aws_route53_record" "private_ceph_client" {
  zone_id = aws_route53_zone.private.id
  name    = "ceph-client.ceph-perf-testcase-pve"
  type    = "A"
  ttl     = "300"
  records = [module.ceph_client_instance.private_ip]
}
