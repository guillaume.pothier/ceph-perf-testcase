#! /bin/sh

set -e

: "${TF_BIN:=terraform}"

PVE_HOST=$(cat state.json \
	| jq -r '.values.root_module.child_modules[]
		| select(.address=="module.pve_instance").resources[]
		| select(.address=="module.pve_instance.aws_instance.this[0]")
		.values.public_dns
	')

PVE_PRIVATE_IP=$(cat state.json \
	| jq -r '.values.root_module.child_modules[]
		| select(.address=="module.pve_instance").resources[]
		| select(.address=="module.pve_instance.aws_instance.this[0]")
		.values.private_ip
	')

CEPH_CLIENT_HOST=$(cat state.json \
	| jq -r '.values.root_module.child_modules[]
		| select(.address=="module.ceph_client_instance").resources[]
		| select(.address=="module.ceph_client_instance.aws_instance.this[0]")
		.values.public_dns
	')

SSH=$(which ssh)
RSYNC=$(which rsync)

ssh_pve() {
	${SSH} -i sshkey admin@${PVE_HOST} $@
}

ssh_ceph_client() {
	${SSH} -i sshkey admin@${CEPH_CLIENT_HOST} $@
}

rsync() {
	${RSYNC} -e "ssh -i sshkey" $@
}

retry() {
	while true
	do
		"$@" && break || sleep 10
	done
}

#
# Setup PVE
#

echo "Accept SSH key for PVE"
ssh_pve -o "StrictHostKeyChecking=accept-new" echo "ok"

echo "Wait until cloudinit has finished setting up everything"
ssh_pve cloud-init status --wait

echo "Install some package for the following parts of the script"
ssh_pve sudo apt-get update
ssh_pve sudo DEBIAN_FRONTEND=noninteractive apt-get -y install \
	moreutils rsync

echo "Copy files to root"
ssh_pve mkdir -p /tmp/pve
rsync -az ./files/pve/ admin@${PVE_HOST}:/tmp/pve/
ssh_pve sudo chown -R root:root /tmp/pve/
ssh_pve sudo rsync -a /tmp/pve/ /

echo "Put private IP in hosts file template"
# The private IP is the only one defined on the host's interface
# (the public IP is probably NATed)
# Proxmox doesn't start if the IP is not defined on the interface,
# even though it is reachable.
HOSTS_TMPL=/etc/cloud/templates/hosts.debian.tmpl
ssh_pve cat ${HOSTS_TMPL} "|" PVE_PRIVATE_IP=${PVE_PRIVATE_IP} envsubst "|" sudo sponge ${HOSTS_TMPL}

echo "Reboot in order to pick up the hosts file template"
ssh_pve sudo reboot || echo "Rebooting"
retry ssh_pve cloud-init status --wait

echo "Install Proxmox VE"
ssh_pve sudo apt-get update
ssh_pve sudo DEBIAN_FRONTEND=noninteractive apt-get -y install \
	proxmox-ve \
    ifupdown2-

# The Proxmox package adds this source, so delete it in case this script
# is executed more than once
ssh_pve sudo rm -f /etc/apt/sources.list.d/pve-enterprise.list

echo "Reboot to pick up new kernel modules"

echo "Install & configure Ceph"
ssh_pve sudo pvecm create testcase
retry ssh_pve sudo pvecm status

echo "Setup Ceph"
ssh_pve echo "yes" "|" sudo pveceph install
ssh_pve sudo pveceph init --network 10.0.129.0/24 --min_size 1 --size 1
ssh_pve sudo pveceph mon create
ssh_pve sudo pveceph osd create /dev/sdf
ssh_pve sudo pveceph pool create rbd --min_size 1 --size 1 --pg_autoscale_mode on
ssh_pve sudo ceph osd pool application enable rbd rbd

echo "Waiting a bit for things to settle..."
sleep 30

# We have no redundancy on the backup instance, so we mute this warning
# so that the health check ends up ok
ssh_pve sudo ceph health mute POOL_NO_REDUNDANCY

echo "Done setting up PVE"

#
# Setting up Ceph client
#

echo "Accept SSH key for Ceph Client"
ssh_ceph_client -o "StrictHostKeyChecking=accept-new" echo "ok"

echo "Wait until cloudinit has finished setting up everything"
ssh_ceph_client cloud-init status --wait

echo "Install some package for the following parts of the script"
ssh_ceph_client sudo apt-get update
ssh_ceph_client sudo DEBIAN_FRONTEND=noninteractive apt-get -y install \
	moreutils rsync pv

echo "Copy files to root"
ssh_ceph_client mkdir -p /tmp/ceph-client
rsync -az ./files/ceph-client/ admin@${CEPH_CLIENT_HOST}:/tmp/ceph-client/
ssh_ceph_client sudo chown -R root:root /tmp/ceph-client/
ssh_ceph_client sudo rsync -a /tmp/ceph-client/ /

echo "Install ceph-common package"
ssh_ceph_client sudo apt-get update
ssh_ceph_client sudo DEBIAN_FRONTEND=noninteractive apt-get -y install \
	ceph-common

echo "Get Ceph config from PVE"
ssh_pve sudo ceph config generate-minimal-conf | \
	ssh_ceph_client cat "|" sudo tee /etc/ceph/ceph.conf ">" /dev/null
ssh_pve sudo ceph auth get-or-create client.admin | \
	ssh_ceph_client sudo cat "|" sudo tee /etc/ceph/ceph.client.admin.keyring ">" /dev/null

echo "Check status"
ssh_ceph_client ceph -s

echo PVE_HOST: ${PVE_HOST}
echo CEPH_CLIENT_HOST: ${CEPH_CLIENT_HOST}
