#! /bin/sh

set -e

echo "Create and map volume (if needed)"
rbd du testcase || rbd create -s 900G testcase
rbd unmap /dev/rbd0 || echo "Volume was not mapped"
RBD_DEV=$(rbd map testcase)

echo "Testing"
head -c 800G </dev/urandom | pv -i 10 -F '%b %t current: %r average: %a' -f 2>&1 >${RBD_DEV}
cat ${RBD_DEV} | pv -i 10 -F '%b %t %r %a' -f 2>&1 >/dev/null






