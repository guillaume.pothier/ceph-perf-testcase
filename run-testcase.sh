#! /bin/sh

set -e

CEPH_CLIENT_HOST=$(cat state.json \
	| jq -r '.values.root_module.child_modules[]
		| select(.address=="module.ceph_client_instance").resources[]
		| select(.address=="module.ceph_client_instance.aws_instance.this[0]")
		.values.public_dns
	')

SSH=$(which ssh)

ssh() {
	${SSH} -i sshkey admin@${CEPH_CLIENT_HOST} $@
}

echo "Copy script to client"
cat run-testcase-on-client.sh | ssh "cat | sudo tee /tmp/run-testcase-on-client.sh > /dev/null"
ssh sudo chmod a+x /tmp/run-testcase-on-client.sh
ssh sudo /tmp/run-testcase-on-client.sh | tr '\r' '\n'
